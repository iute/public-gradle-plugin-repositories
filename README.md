[TOC]

# Public Gradle Plugins

Latest versions (`0.21.0`, `0.22.0`) of `com.commercehub.gradle.plugin:gradle-avro-plugin` Gradle plugin are published only in JCenter.
See https://mvnrepository.com/artifact/com.commercehub.gradle.plugin/gradle-avro-plugin?repo=jcenter
JCenter will be shutdown at 1 May. This repository is a backup plan till `com.commercehub.gradle.plugin:gradle-avro-plugin` maintainers switch to Public Maven repository.

Please update your `settings.gradle`

## Migration

OLD
```
maven {
    url 'https://dl.bintray.com/gradle/gradle-plugins' // for avro plugin
}
```

NEW

```
maven {
    url 'https://bitbucket.org/iute/public-gradle-plugin-repositories/raw/master/repo' // for avro plugin
}
```

## Maintain

Maven repository layout is done by [public-gradle-plugins](https://bitbucket.org/iute/public-gradle-plugins/src/master/)